﻿using OpenVisualizationTK.DataProviders;
using OpenTK.Graphics.ES11;
using Xamarin.Forms;

namespace OpenVisualizationTK.Viewers
{
    public class Viewer2D : ContentView
    {
        private readonly DataProvider2D dataProvider;
        private readonly OpenGLView view;

        public DataProvider2D DataProvider => dataProvider;

        public Viewer2D()
            : this(new DataProvider2D())
        {
        }

        public Viewer2D(DataProvider2D dataProvider)
        {
            this.dataProvider = dataProvider;
            this.dataProvider.DataReceived += OnDataReceived;

            view = new OpenGLView();
            view.HasRenderLoop = true;
            view.HeightRequest = 300;
            view.WidthRequest = 300;
            view.OnDisplay = OnRenderData;

            Content = view;
        }

        private void OnRenderData(Rectangle obj)
        {
            if (DataProvider.Data == null)
            {
                return;
            }

            float[] squareVertices = CreateVertices(dataProvider.ResolutionX, dataProvider.ResolutionY);
            byte[] squareColors = ColorizeData(dataProvider.Data);
            int size = DataProvider.ResolutionX;

            //MakeCurrent();
            GL.Viewport(0, 0, (int)view.Width, (int)view.Height);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            //GL.Ortho(1f, 1f, 1f, 1f, 1f, 1f);
            GL.MatrixMode(MatrixMode.Modelview);
            //GL.Rotate(3.0f, 0.0f, 0.0f, 1.0f);

            GL.ClearColor(0.5f, 0.5f, 0.5f, 1.0f);
            GL.Clear(ClearBufferMask.ColorBufferBit);

            GL.VertexPointer(2, VertexPointerType.Float, 0, squareVertices);
            GL.EnableClientState(EnableCap.VertexArray);

            GL.ColorPointer(4, ColorPointerType.UnsignedByte, 0, squareColors);
            GL.EnableClientState(EnableCap.ColorArray);
            GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);
        }

        private float[] CreateVertices(int resX, int resY)
        {
            float[] squareVertices =
                {
                    -1f, -1f,
                    1f, -1f,
                    -1f, 1f,
                    1f, 1f,
                };

            return squareVertices;
        }

        private byte[] ColorizeData(byte[] data)
        {
            byte[] colors = new byte[data.Length * 4];

            for (int i = 0; i < data.Length; i++)
            {
                colors[4 * i] = data[i];
                colors[4 * i + 1] = 0;
                colors[4 * i + 2] = 0;
                colors[4 * i + 3] = 255;
            }

            return colors;
        }

        private void OnDataReceived(object sender, ReceivedDataEventArgs e)
        {
            view.Display();
        }
    }
}
