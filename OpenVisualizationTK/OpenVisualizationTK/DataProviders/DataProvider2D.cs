﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenVisualizationTK.DataProviders
{
    public class DataProvider2D
    {
        public event EventHandler<ReceivedDataEventArgs> DataReceived;

        public int ResolutionX { get; set; }
        public int ResolutionY { get; set; }
        public byte[] Data { get; private set; }

        public void SetData(byte[] data)
        {
            ThrowIfDataCanNotBeSet();

            Data = data;
            DataReceived?.Invoke(this, new ReceivedDataEventArgs(data, ResolutionX, ResolutionY));
        }

        private void ThrowIfDataCanNotBeSet()
        {
            if (ResolutionX <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(ResolutionX));
            }
            if (ResolutionY <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(ResolutionY));
            }
        }
    }

    public class ReceivedDataEventArgs : EventArgs
    {
        public byte[] Data { get; }
        public int ResolutionX { get; }
        public int ResolutionY { get; }

        public ReceivedDataEventArgs(byte[] data, int resX, int resY)
        {
            Data = data;
            ResolutionX = resX;
            ResolutionY = resY;
        }
    }
}
